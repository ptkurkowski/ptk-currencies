package com.pkurkowski.ptkcurrencies.activities.currency

import android.databinding.ObservableDouble
import com.pkurkowski.ptkcurrencies.data.logic.ObservableCurrency
import com.pkurkowski.ptkcurrencies.injection.ActivityScope
import com.pkurkowski.ptkcurrencies.injection.SchedulerModule
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.subjects.Subject
import javax.inject.Named

@Module
class CurrenciesActivityModule {

    @ActivityScope
    @Provides
    fun providePresenter(
        ratesProvider: Observable<CurrenciesActivityViewState>,
        restartSubject: Subject<Unit>,
        baseValue: ObservableDouble,
        selectedCurrency: ObservableCurrency,
        @Named(SchedulerModule.IO_SCHEDULER) schedulerIo: Scheduler
    ) = CurrenciesPresenter(
        ratesProvider,
        restartSubject,
        baseValue,
        selectedCurrency,
        schedulerIo
    )
}