package com.pkurkowski.ptkcurrencies.activities.currency.ui

interface TextViewDataInterface {

    val stringData:String
    var valueData: Double
    val focused: Boolean

}