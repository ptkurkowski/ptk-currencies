package com.pkurkowski.ptkcurrencies.activities.currency.ui

import com.pkurkowski.ptkcurrencies.R
import com.pkurkowski.ptkcurrencies.data.model.Currency

object CurrencyResourceConverter {

    @JvmStatic
    fun getName(currency: Currency)=
        when(currency) {
            Currency.EUR -> R.string.currency_eur

            Currency.AUD -> R.string.currency_aud
            Currency.BGN -> R.string.currency_bgn
            Currency.BRL -> R.string.currency_brl
            Currency.CAD -> R.string.currency_cad
            Currency.CHF -> R.string.currency_chf

            Currency.CNY -> R.string.currency_cny
            Currency.CZK -> R.string.currency_czk
            Currency.DKK -> R.string.currency_dkk
            Currency.GBP -> R.string.currency_gbp
            Currency.HKD -> R.string.currency_hkd

            Currency.HRK -> R.string.currency_hrk
            Currency.HUF -> R.string.currency_huf
            Currency.IDR -> R.string.currency_idr
            Currency.ILS -> R.string.currency_ils
            Currency.INR -> R.string.currency_inr

            Currency.ISK -> R.string.currency_isk
            Currency.JPY -> R.string.currency_jpy
            Currency.KRW -> R.string.currency_krw
            Currency.MXN -> R.string.currency_mxn
            Currency.MYR -> R.string.currency_myr

            Currency.NOK -> R.string.currency_nok
            Currency.NZD -> R.string.currency_nzd
            Currency.PHP -> R.string.currency_php
            Currency.PLN -> R.string.currency_pln
            Currency.RON -> R.string.currency_ron

            Currency.RUB -> R.string.currency_rub
            Currency.SEK -> R.string.currency_sek
            Currency.SGD -> R.string.currency_sgd
            Currency.THB -> R.string.currency_thb
            Currency.TRY -> R.string.currency_try

            Currency.USD -> R.string.currency_usd
            Currency.ZAR -> R.string.currency_zar
        }


    @JvmStatic
    fun getFlagDrawable(currency: Currency)=
        when(currency) {
            Currency.EUR -> R.drawable.flag_eur

            Currency.AUD -> R.drawable.flag_aud
            Currency.BGN -> R.drawable.flag_bgn
            Currency.BRL -> R.drawable.flag_brl
            Currency.CAD -> R.drawable.flag_cad
            Currency.CHF -> R.drawable.flag_chf

            Currency.CNY -> R.drawable.flag_cny
            Currency.CZK -> R.drawable.flag_czk
            Currency.DKK -> R.drawable.flag_dkk
            Currency.GBP -> R.drawable.flag_gbp
            Currency.HKD -> R.drawable.flag_hkd

            Currency.HRK -> R.drawable.flag_hrk
            Currency.HUF -> R.drawable.flag_huf
            Currency.IDR -> R.drawable.flag_idr
            Currency.ILS -> R.drawable.flag_ils
            Currency.INR -> R.drawable.flag_inr

            Currency.ISK -> R.drawable.flag_isk
            Currency.JPY -> R.drawable.flag_jpy
            Currency.KRW -> R.drawable.flag_krw
            Currency.MXN -> R.drawable.flag_mxn
            Currency.MYR -> R.drawable.flag_myr

            Currency.NOK -> R.drawable.flag_nok
            Currency.NZD -> R.drawable.flag_nzd
            Currency.PHP -> R.drawable.flag_php
            Currency.PLN -> R.drawable.flag_pln
            Currency.RON -> R.drawable.flag_ron

            Currency.RUB -> R.drawable.flag_rub
            Currency.SEK -> R.drawable.flag_sek
            Currency.SGD -> R.drawable.flag_sgd
            Currency.THB -> R.drawable.flag_thb
            Currency.TRY -> R.drawable.flag_try

            Currency.USD -> R.drawable.flag_usd
            Currency.ZAR -> R.drawable.flag_zar
        }
    
}