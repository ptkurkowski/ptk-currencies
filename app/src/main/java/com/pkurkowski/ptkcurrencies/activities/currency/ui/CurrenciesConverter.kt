package com.pkurkowski.ptkcurrencies.activities.currency.ui

import android.databinding.InverseMethod
import com.pkurkowski.ptkcurrencies.BuildConfig
import com.pkurkowski.ptkcurrencies.data.model.CurrencyRate
import com.pkurkowski.ptkcurrencies.extension.applyScale
import com.pkurkowski.ptkcurrencies.extension.getDefaultString
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

object CurrenciesConverter {

    private val MATH_CONTEXT = MathContext(BuildConfig.PRECISION, RoundingMode.HALF_EVEN)


    @InverseMethod("stringToData")
    @JvmStatic
    fun dataToString(view: TextViewDataInterface, item: CurrencyRate, value: Double): String {
        view.valueData = value

        return if (view.focused) {
            view.stringData
        } else {
            return multiplyWithDigDecimal(
                value,
                item.rate
            ).applyScale()
            .getDefaultString()
        }
    }

    @JvmStatic
    fun stringToData(view: TextViewDataInterface, item: CurrencyRate, string: String): Double =
        if (!view.focused) {
            view.valueData
        } else {
            parseOrReturnZero(
                string
            )
                .divide(
                    BigDecimal(item.rate),
                    MATH_CONTEXT
                )
                .toDouble()
        }

    fun multiplyWithDigDecimal(valueOne: Double, valueTwo: Double): BigDecimal =
        BigDecimal(valueOne)
            .multiply(
                BigDecimal(valueTwo),
                MATH_CONTEXT
            )

    private fun parseOrReturnZero(string: String): BigDecimal {
        return try {
            BigDecimal(
                string,
                MATH_CONTEXT
            )
        } catch (e: NumberFormatException) {
            BigDecimal.ZERO
        }
    }

    fun calculateNewValue(oldRate: Double, newRate: Double, oldValue: Double): Double {
        return BigDecimal(newRate)
            .divide(BigDecimal(oldRate), MATH_CONTEXT)
            .multiply(BigDecimal(oldValue), MATH_CONTEXT)
            .toDouble()
    }
}