package com.pkurkowski.ptkcurrencies.activities.currency

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.pkurkowski.ptkcurrencies.R
import com.pkurkowski.ptkcurrencies.activities.currency.ui.CurrenciesAdapter
import com.pkurkowski.ptkcurrencies.databinding.ActivityCurrienciesBinding
import dagger.android.AndroidInjection
import javax.inject.Inject

class CurrenciesActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: CurrenciesPresenter
    private lateinit var binding: ActivityCurrienciesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_curriencies)
        binding.data = CurrenciesActivityViewState.LoadingData

        binding.recycledView.layoutManager = LinearLayoutManager(this)
        binding.recycledView.adapter =
            CurrenciesAdapter(presenter.baseValue, presenter.selectedCurrency)
    }

    override fun onResume() {
        super.onResume()
        presenter.start(binding)

    }

    override fun onPause() {
        presenter.stop()
        super.onPause()
    }
}
