package com.pkurkowski.ptkcurrencies.activities.currency.ui


import android.databinding.ObservableDouble
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.pkurkowski.ptkcurrencies.BuildConfig
import com.pkurkowski.ptkcurrencies.data.logic.ObservableCurrency
import com.pkurkowski.ptkcurrencies.data.model.Currency
import com.pkurkowski.ptkcurrencies.data.model.CurrencyRate
import com.pkurkowski.ptkcurrencies.databinding.ItemCurrencyBinding

class CurrenciesAdapter(
    val currentValue: ObservableDouble,
    val selectedCurrency: ObservableCurrency
) :
    RecyclerView.Adapter<CurrenciesAdapter.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    private var originalCurrenciesData: List<CurrencyRate> = emptyList()
    private var modifiedCurrenciesData: List<CurrencyRate> = emptyList()

    fun update(items: List<CurrencyRate>) {
        calculateNewValue(this.originalCurrenciesData, items)
        this.originalCurrenciesData = items
        swapItemToTop()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCurrencyBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = modifiedCurrenciesData.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(modifiedCurrenciesData[position])

    override fun getItemId(position: Int): Long {
        return this.modifiedCurrenciesData[position].currency.ordinal.toLong()
    }

    inner class ViewHolder(val binding: ItemCurrencyBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CurrencyRate) {

            binding.valueEditText.filters = arrayOf(getFilterForItem(item))
            binding.item = item
            binding.observableBaseValue = currentValue
            binding.executePendingBindings()

            if (item.currency == selectedCurrency.currency) binding.valueEditText.requestFocus()

            binding.mainLayout.setOnClickListener {
                if (selectedCurrency.currency != item.currency) {
                    selectedCurrency.currency = item.currency
                    swapItemToTop(false)
                    binding.valueEditText.requestFocus()
                }
            }
        }
    }

    private fun swapItemToTop(newData: Boolean = true) {
        val selectedIndex =
            originalCurrenciesData.indexOfFirst { it.currency == selectedCurrency.currency }
        when (selectedIndex) {
            -1 -> modifiedCurrenciesData = originalCurrenciesData
            0 -> {
                val currentIndex = modifiedCurrenciesData.indexOfFirst { it.currency == selectedCurrency.currency }
                modifiedCurrenciesData = originalCurrenciesData
                if (!newData && currentIndex != -1) {
                    notifyItemMoved(currentIndex, 0)
                }
            }
            else -> {
                modifiedCurrenciesData = originalCurrenciesData.toMutableList().also {
                    it.add(0, it.removeAt(selectedIndex))
                }
                notifyItemMoved(selectedIndex, 0)
            }
        }
    }


    private fun getFilterForItem(item: CurrencyRate): DecimalFilter {
        return DecimalFilter(
            maxValue = CurrenciesConverter.multiplyWithDigDecimal(
                BuildConfig.MAX_BASE_VALUE,
                item.rate
            ).toDouble(),
            maxDigitAfterPeriod = BuildConfig.SCALE
        )
    }

    private fun calculateNewValue(
        oldItems: List<CurrencyRate>?,
        newItems: List<CurrencyRate>
    ) {
        if (oldItems == null) return
        val currency = selectedCurrency.currency
        val oldCurrencyRate = getCurrencyRateFromItems(currency, oldItems)
        val newCurrencyRate = getCurrencyRateFromItems(currency, newItems)

        if (oldCurrencyRate == null || newCurrencyRate == null) return
        currentValue.set( CurrenciesConverter.calculateNewValue(
            newCurrencyRate.rate,
            oldCurrencyRate.rate,
            currentValue.get()
        ))

    }

    private fun getCurrencyRateFromItems(currency: Currency, items: List<CurrencyRate>) =
        items.firstOrNull { currencyRate -> currencyRate.currency === currency }

}