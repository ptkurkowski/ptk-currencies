package com.pkurkowski.ptkcurrencies.activities.currency.ui

import android.text.InputFilter
import android.text.Spanned
import com.pkurkowski.ptkcurrencies.extension.applyScale
import com.pkurkowski.ptkcurrencies.extension.getDefaultString

import java.math.BigDecimal
import java.util.regex.Pattern

class DecimalFilter(val maxValue: Double, val maxDigitAfterPeriod: Int) : InputFilter {

    private val pattern = Pattern.compile(
        "-?[0-9]{0,}+((\\.[0-9]{0," + (maxDigitAfterPeriod)
                + "})?)||(\\.)?"
    )

    private val maxValueString = BigDecimal(maxValue).applyScale().getDefaultString()

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val replacement = source.subSequence(start, end).toString()

        val newVal = dest.subSequence(0, dstart).toString() +
                replacement +
                dest.subSequence(dend, dest.length).toString()
        val matcher = pattern.matcher(newVal)

        return when {
            matcher.matches() && isSmallerThenMax(newVal) -> null
            dest.isEmpty() -> maxValueString
            else -> ""
        }

    }

    private fun isSmallerThenMax(string: String): Boolean {
        string.toDoubleOrNull()?.let {
            return it <= maxValue
        }

        return true
    }

}