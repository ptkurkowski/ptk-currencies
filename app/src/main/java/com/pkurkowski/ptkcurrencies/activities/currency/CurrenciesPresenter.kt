package com.pkurkowski.ptkcurrencies.activities.currency

import android.databinding.ObservableDouble
import com.pkurkowski.ptkcurrencies.BuildConfig
import com.pkurkowski.ptkcurrencies.data.logic.ObservableCurrency
import com.pkurkowski.ptkcurrencies.databinding.ActivityCurrienciesBinding
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.Subject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class CurrenciesPresenter(
    private val ratesProvider: Observable<CurrenciesActivityViewState>,
    private val restartSubject: Subject<Unit>,
    val baseValue: ObservableDouble,
    val selectedCurrency: ObservableCurrency,
    private val schedulerIo: Scheduler
) {
    private val compositeDisposable = CompositeDisposable()

    fun start(binding: ActivityCurrienciesBinding) {
        compositeDisposable.clear()
        compositeDisposable.add(Observable
            .interval(BuildConfig.RELOAD_DELAY_SEC, BuildConfig.RELOAD_DELAY_SEC, TimeUnit.SECONDS)
            .observeOn(schedulerIo)
            .forEach {
                restartSubject.onNext(Unit)
            })

        compositeDisposable.add(ratesProvider
            .subscribeOn(schedulerIo)
            .subscribe {
                Timber.d("render data: ${it::class.java.simpleName}")
                binding.data = it
            })
    }

    fun stop() {
        compositeDisposable.clear()
    }

}