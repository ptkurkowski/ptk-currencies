package com.pkurkowski.ptkcurrencies.activities.currency

import android.databinding.BindingAdapter
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.pkurkowski.ptkcurrencies.activities.currency.ui.CurrenciesAdapter
import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList

sealed class CurrenciesActivityViewState {
    object LoadingData : CurrenciesActivityViewState()

    data class ErrorStateNoData(override val throwable: Throwable) : CurrenciesActivityViewState(),
        ThrowableDataProvider

    data class DataLoaded(override val data: CurrenciesRatesList) : CurrenciesActivityViewState(),
        RecyclerViewDataProvider

    data class ErrorStateWeHaveOlderData(
        override val data: CurrenciesRatesList,
        override val throwable: Throwable
    ) : CurrenciesActivityViewState(), RecyclerViewDataProvider, ThrowableDataProvider

    interface RecyclerViewDataProvider {
        val data: CurrenciesRatesList
    }

    interface ThrowableDataProvider {
        val throwable: Throwable
    }

}

@BindingAdapter("recyclerState")
fun setRecyclerVisibility(recyclerView: RecyclerView, state: CurrenciesActivityViewState) {
    when (state) {
        is CurrenciesActivityViewState.RecyclerViewDataProvider -> {
            recyclerView.visibility = View.VISIBLE
            processRecyclerDataAndAdapter(recyclerView, state.data)
        }
        else -> recyclerView.visibility = View.GONE
    }
}

@BindingAdapter("informationLayoutState")
fun setProgressVisibility(layout: ConstraintLayout, state: CurrenciesActivityViewState) {
    layout.visibility = when (state) {
        is CurrenciesActivityViewState.DataLoaded -> View.GONE
        else -> View.VISIBLE
    }

    layout.setBackgroundResource(
        when (state) {
            is CurrenciesActivityViewState.RecyclerViewDataProvider -> android.R.color.darker_gray
            else -> android.R.color.transparent
        }
    )

}

@BindingAdapter("statusTextState")
fun setStatusText(statusTextView: TextView, state: CurrenciesActivityViewState) {
    when (state) {
        is CurrenciesActivityViewState.ThrowableDataProvider -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.text = state.throwable.toString()
        }
        else -> statusTextView.visibility = View.GONE
    }
}

private fun processRecyclerDataAndAdapter(recyclerView: RecyclerView, data: CurrenciesRatesList) {
    val adapter = recyclerView.adapter
    if (adapter is CurrenciesAdapter) {
        adapter.update(data.rates)
    }
}