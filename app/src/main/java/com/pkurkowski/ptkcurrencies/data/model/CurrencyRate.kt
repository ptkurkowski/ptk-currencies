package com.pkurkowski.ptkcurrencies.data.model

data class CurrencyRate(val currency: Currency, val rate: Double)