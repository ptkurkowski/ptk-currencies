package com.pkurkowski.ptkcurrencies.data.logic

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.android.databinding.library.baseAdapters.BR
import com.pkurkowski.ptkcurrencies.data.model.Currency

class ObservableCurrency(startValue: Currency): BaseObservable() {

    @get:Bindable
    var currency: Currency = startValue
        set(value) {
            field = value
            notifyPropertyChanged(BR.currency)
        }

}