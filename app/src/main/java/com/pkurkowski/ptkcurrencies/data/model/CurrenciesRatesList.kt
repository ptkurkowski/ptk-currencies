package com.pkurkowski.ptkcurrencies.data.model

import java.util.*

data class CurrenciesRatesList(val base: Currency, val date:Date, val rates: List<CurrencyRate>)