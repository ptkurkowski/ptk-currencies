package com.pkurkowski.ptkcurrencies.data.remote

import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("latest")
    fun getLatest(
        @Query("base") base: String
    ): Single<CurrenciesRatesList>

}