package com.pkurkowski.ptkcurrencies.data.remote

import com.google.gson.*
import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import com.pkurkowski.ptkcurrencies.data.model.Currency
import com.pkurkowski.ptkcurrencies.data.model.CurrencyRate
import java.lang.reflect.Type
import java.util.*

class CurrencyRateListDeserializer : JsonDeserializer<CurrenciesRatesList> {
    val BASE = "base"
    val DATE = "date"
    val RATES = "rates"

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): CurrenciesRatesList {

        val currenciesList = mutableListOf<CurrencyRate>()
        val topJsonObj = json.asJsonObject
        val ratesJsonObj = topJsonObj.get(RATES).asJsonObject

        ratesJsonObj.keySet().forEach {
            it?.let {
                processObject(it, ratesJsonObj.get(it), currenciesList)
            }
        }

        val baseCurrency = getBase(topJsonObj)!!
        currenciesList.add(0, CurrencyRate(baseCurrency, 1.0))

        return CurrenciesRatesList(baseCurrency, getDate(topJsonObj, context), currenciesList.toList())
    }

    private fun getBase(jsonObj: JsonObject) = Currency.getByStringSymbol(jsonObj.get(BASE).asString)

    private fun getDate(jsonObj: JsonObject, context: JsonDeserializationContext): Date {
        return context.deserialize(jsonObj.get(DATE), Date::class.java)
    }

    private fun processObject(name: String, data: JsonElement?, result: MutableList<CurrencyRate>) {
        data?.run {
            if (this is JsonPrimitive) {
                Currency.getByStringSymbol(name)?.let {
                    result.add(CurrencyRate(it, this.asDouble))
                }
            }
        }
    }
}