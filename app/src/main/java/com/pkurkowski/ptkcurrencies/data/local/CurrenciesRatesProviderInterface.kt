package com.pkurkowski.ptkcurrencies.data.local

import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import io.reactivex.Single

interface CurrenciesRatesProviderInterface {
    fun request(spec: CurrenciesRatesSpecification): Single<CurrenciesRatesList>
}

sealed class CurrenciesRatesSpecification {
    object EUROBaseSpecification : CurrenciesRatesSpecification()
}