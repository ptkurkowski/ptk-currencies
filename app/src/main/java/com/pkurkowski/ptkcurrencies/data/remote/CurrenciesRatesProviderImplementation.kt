package com.pkurkowski.ptkcurrencies.data.remote

import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesProviderInterface
import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesSpecification
import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import com.pkurkowski.ptkcurrencies.data.model.Currency
import io.reactivex.Single

class CurrenciesRatesProviderImplementation(private val implementation: ApiInterface) : CurrenciesRatesProviderInterface {

    override fun request(spec: CurrenciesRatesSpecification): Single<CurrenciesRatesList> =
        when(spec) {
            is CurrenciesRatesSpecification.EUROBaseSpecification -> implementation.getLatest(Currency.EUR.symbol)
        }
}