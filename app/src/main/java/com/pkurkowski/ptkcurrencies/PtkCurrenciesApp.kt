package com.pkurkowski.ptkcurrencies

import android.app.Application
import com.pkurkowski.ptkcurrencies.injection.DaggerPtkCurrenciesAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject


class PtkCurrenciesApp: Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        DaggerPtkCurrenciesAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }


    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector
}