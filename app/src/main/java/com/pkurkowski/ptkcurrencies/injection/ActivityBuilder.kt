package com.pkurkowski.ptkcurrencies.injection

import com.pkurkowski.ptkcurrencies.activities.currency.CurrenciesActivity
import com.pkurkowski.ptkcurrencies.activities.currency.CurrenciesActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [CurrenciesActivityModule::class])
    internal abstract fun bindMainActivity(): CurrenciesActivity

}