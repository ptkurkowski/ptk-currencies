package com.pkurkowski.ptkcurrencies.injection

import android.app.Application
import com.pkurkowski.ptkcurrencies.PtkCurrenciesApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [CurrenciesDataModule::class, CurrenciesCommunicationModule::class, SchedulerModule::class, AndroidInjectionModule::class, ActivityBuilder::class])
interface PtkCurrenciesAppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): PtkCurrenciesAppComponent
    }

    fun inject(app: PtkCurrenciesApp)
}