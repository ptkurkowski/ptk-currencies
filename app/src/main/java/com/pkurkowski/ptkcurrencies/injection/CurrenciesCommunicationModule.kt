package com.pkurkowski.ptkcurrencies.injection

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pkurkowski.ptkcurrencies.BuildConfig
import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesProviderInterface
import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import com.pkurkowski.ptkcurrencies.data.remote.ApiInterface
import com.pkurkowski.ptkcurrencies.data.remote.CurrenciesRatesProviderImplementation
import com.pkurkowski.ptkcurrencies.data.remote.CurrencyRateListDeserializer
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class CurrenciesCommunicationModule {

    @Provides
    @Singleton
    internal fun providesGson() = GsonBuilder()
        .registerTypeAdapter(CurrenciesRatesList::class.java, CurrencyRateListDeserializer())
        .create()

    @Provides
    @Singleton
    internal fun providesConverterFactory(gson: Gson) = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    internal fun providesRxJavaCallAdapterFactory(@Named(SchedulerModule.IO_SCHEDULER) schedulerIo: Scheduler) =
        RxJava2CallAdapterFactory.createWithScheduler(schedulerIo)

    @Provides
    @Singleton
    internal fun providesHttpClient(): OkHttpClient {

        return OkHttpClient.Builder().also {
            if (BuildConfig.DEBUG) it.addInterceptor(
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            )
        }.build()

    }

    @Provides
    @Singleton
    fun providesCurrenciesRatesProvider(
        converterFactory: GsonConverterFactory,
        okHttp: OkHttpClient,
        rxJavaCallAdapterFactory: RxJava2CallAdapterFactory
    ): CurrenciesRatesProviderInterface {
        return CurrenciesRatesProviderImplementation(
            Retrofit.Builder()
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .baseUrl(BuildConfig.API_URL)
                .client(okHttp)
                .build()
                .create(ApiInterface::class.java)
        )
    }

}