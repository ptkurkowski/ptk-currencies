package com.pkurkowski.ptkcurrencies.injection

import android.databinding.ObservableDouble
import com.pkurkowski.ptkcurrencies.activities.currency.CurrenciesActivityViewState
import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesProviderInterface
import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesSpecification
import com.pkurkowski.ptkcurrencies.data.logic.ObservableCurrency
import com.pkurkowski.ptkcurrencies.data.model.Currency
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Singleton

@Module
class CurrenciesDataModule {

    @Provides
    @Singleton
    fun providesBaseValueObservable() = ObservableDouble(1.0)

    @Provides
    @Singleton
    fun providesSelectedCurrency() =
        ObservableCurrency(Currency.EUR)

    @Provides
    @Singleton
    fun providesRestartSubject(): Subject<Unit> =
        PublishSubject.create<Unit>()


    @Singleton
    @Provides
    fun provideRatesProvider(
        currenciesProvider: CurrenciesRatesProviderInterface,
        restartSubject: Subject<Unit>
    ): Observable<CurrenciesActivityViewState> {


        return BehaviorSubject.create<CurrenciesActivityViewState>().also { subject ->
            currenciesProvider
                .request(CurrenciesRatesSpecification.EUROBaseSpecification)
                .map { CurrenciesActivityViewState.DataLoaded(it) }
                .cast(CurrenciesActivityViewState::class.java)
                .onErrorReturn { CurrenciesActivityViewState.ErrorStateNoData(it) }
                .toObservable()
                .repeatWhen { restartSubject }
                .scan { oldState, newState ->
                    if (oldState is CurrenciesActivityViewState.RecyclerViewDataProvider &&
                        newState is CurrenciesActivityViewState.ThrowableDataProvider
                    ) {
                        CurrenciesActivityViewState.ErrorStateWeHaveOlderData(
                            oldState.data,
                            newState.throwable
                        )
                    } else {
                        newState
                    }
                }
                .subscribe(subject)
        }.startWith(CurrenciesActivityViewState.LoadingData)
    }

}