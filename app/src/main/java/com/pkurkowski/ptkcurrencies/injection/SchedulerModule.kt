package com.pkurkowski.ptkcurrencies.injection

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton


@Module
class SchedulerModule {


    @Provides
    @Named(IO_SCHEDULER)
    @Singleton
    fun providesBackgroundScheduler() = Schedulers.io() as Scheduler

    @Provides
    @Singleton
    @Named(UI_SCHEDULER)
    fun providesUiScheduler() = AndroidSchedulers.mainThread() as Scheduler


    companion object {
        const val IO_SCHEDULER = "IO_SCHEDULER"
        const val UI_SCHEDULER = "UI_SCHEDULER"
    }

}