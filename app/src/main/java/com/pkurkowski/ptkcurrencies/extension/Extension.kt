package com.pkurkowski.ptkcurrencies.extension

import com.pkurkowski.ptkcurrencies.BuildConfig
import java.math.BigDecimal


fun BigDecimal.applyScale() =
    this.setScale(BuildConfig.SCALE, BigDecimal.ROUND_HALF_EVEN)

//fix for https://stackoverflow.com/questions/34414785/bigdecimal-striptrailingzeros-doesnt-work-for-zero/34414905
fun BigDecimal.getDefaultString() =
    if (this.compareTo(BigDecimal.ZERO) == 0) BigDecimal.ZERO.toPlainString()
    else this.stripTrailingZeros().toPlainString()
