package com.pkurkowski.ptkcurrencies.ui

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText
import com.pkurkowski.ptkcurrencies.activities.currency.ui.TextViewDataInterface

class CustomDataAndTouchEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : EditText(context, attrs, defStyleAttr), TextViewDataInterface {

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent) =
        if (hasFocus()) super.onTouchEvent(event)
        else false


    override val stringData: String
        get() = text.toString()

    override var valueData: Double = 1.0

    override val focused: Boolean
        get() = isFocused




}