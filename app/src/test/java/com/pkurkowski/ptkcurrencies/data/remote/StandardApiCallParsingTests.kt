package com.pkurkowski.ptkcurrencies.data.remote

import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import com.pkurkowski.ptkcurrencies.data.model.Currency
import com.pkurkowski.ptkcurrencies.injection.CurrenciesCommunicationModule
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import java.io.BufferedReader
import java.io.InputStreamReader

class StandardApiCallParsingTests {

    lateinit var module: CurrenciesCommunicationModule

    @Before
    fun setUp() {
        module = CurrenciesCommunicationModule()
    }

    @Test
    @Throws(Exception::class)
    fun `after parsing standard response should have needed data`() {
        val inStream = this.javaClass.getResourceAsStream("StandardResponse.json")!!
        val data = module.providesGson().fromJson(
            BufferedReader( InputStreamReader(inStream)),
            CurrenciesRatesList::class.java
        )
        Assertions.assertThat(data).isNotNull

        Currency.values().forEach {currency ->
            Assertions.assertThat(
                data.rates.firstOrNull{
                    it.currency == currency
                }
            ).isNotNull

        }
    }

}