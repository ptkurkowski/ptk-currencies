package com.pkurkowski.ptkcurrencies

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.pkurkowski.ptkcurrencies.activities.currency.ui.CurrenciesConverter
import com.pkurkowski.ptkcurrencies.activities.currency.ui.TextViewDataInterface
import com.pkurkowski.ptkcurrencies.utils.CurrenciesRatesListGenerator
import org.junit.Test
import java.util.concurrent.ThreadLocalRandom

class CurrenciesConverterTests {

    @Test
    fun `translating from and to string should give the same result `() {

        val data = CurrenciesRatesListGenerator.generateCurrenciesRatesList()
        val textViewDataInterfaceFocuses = mock<TextViewDataInterface> {
            on { focused } doReturn true
        }
        val textViewDataInterfaceNotFocuses = mock<TextViewDataInterface> {
            on { focused } doReturn false
        }

        repeat(1000) {pass ->
            val initValue = ThreadLocalRandom.current().nextDouble(0.0001, 99999.9999)
            var value = initValue
            data.rates.forEach {currencyRate ->

                val stringValue = CurrenciesConverter.dataToString(textViewDataInterfaceNotFocuses, currencyRate, value)
                value = CurrenciesConverter.stringToData(textViewDataInterfaceFocuses, currencyRate, stringValue)
                val secondStringValue = CurrenciesConverter.dataToString(textViewDataInterfaceNotFocuses, currencyRate, value)

                assert(stringValue == secondStringValue) {
                    "string values not equal stringValue $stringValue secondStringValue: $secondStringValue\n pass: $pass"
                }

            }
        }
    }

}