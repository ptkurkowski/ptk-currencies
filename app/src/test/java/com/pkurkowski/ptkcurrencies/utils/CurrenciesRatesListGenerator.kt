package com.pkurkowski.ptkcurrencies.utils

import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import com.pkurkowski.ptkcurrencies.data.model.Currency
import com.pkurkowski.ptkcurrencies.data.model.CurrencyRate
import java.util.*
import java.util.concurrent.ThreadLocalRandom

object CurrenciesRatesListGenerator {

    fun generateCurrenciesRatesList() = CurrenciesRatesList(Currency.EUR, Date(),
        Currency.values().map { currency ->
            CurrencyRate(
                currency,
                ThreadLocalRandom.current().nextDouble(0.0001, 99999.9999)
            )
        }
    )
}

