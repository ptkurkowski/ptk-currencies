package com.pkurkowski.ptkcurrencies.injection

import io.reactivex.Scheduler
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test

class SchedulerModuleTests {

    lateinit var module: SchedulerModule

    @Before
    fun setUp() {
        module = SchedulerModule()
    }

    @Test
    @Throws(Exception::class)
    fun `providesBackgroundScheduler instance returns Scheduler`() {
        val provide = module.providesBackgroundScheduler()
        Assertions.assertThat(provide).isInstanceOf(Scheduler::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `providesUiScheduler instance returns Scheduler`() {
        val provide = module.providesBackgroundScheduler()
        Assertions.assertThat(provide).isInstanceOf(Scheduler::class.java)
    }

}