package com.pkurkowski.ptkcurrencies.injection

import android.databinding.ObservableDouble
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.pkurkowski.ptkcurrencies.activities.currency.CurrenciesActivityViewState
import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesProviderInterface
import com.pkurkowski.ptkcurrencies.data.model.CurrenciesRatesList
import com.pkurkowski.ptkcurrencies.utils.CurrenciesRatesListGenerator
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import kotlin.reflect.KClass


class CurrenciesDataModuleTests {

    lateinit var module: CurrenciesDataModule

    @Before
    fun setUp() {
        module = CurrenciesDataModule()
    }

    @Test
    @Throws(Exception::class)
    fun `providesBaseValueObservable returns exactly ObservableDouble`() {
        val provide = module.providesBaseValueObservable()
        Assertions.assertThat(provide).isExactlyInstanceOf(ObservableDouble::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `providesRestartSubject returns exactly PublishSubject`() {
        val provide = module.providesRestartSubject()
        Assertions.assertThat(provide).isExactlyInstanceOf(PublishSubject::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `provideRatesProvider returns  Observable `() {
        val provide = module.provideRatesProvider(mock {
            on { request(any()) } doReturn Single.just(CurrenciesRatesListGenerator.generateCurrenciesRatesList())
        }, module.providesRestartSubject())
        Assertions.assertThat(provide).isInstanceOf(Observable::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `provideRatesProvider behavior test `() {

        val data0 = CurrenciesRatesListGenerator.generateCurrenciesRatesList()
        val data1 = CurrenciesRatesListGenerator.generateCurrenciesRatesList()
        val data2 = CurrenciesRatesListGenerator.generateCurrenciesRatesList()

        val exception0 = Exception("Test exception 0")
        val exception1 = Exception("Test exception 1")

        val dataSubject = PublishSubject.create<Any>()
        val mockProvider = mock<CurrenciesRatesProviderInterface> {
            on { request(any()) } doReturn Single.defer {
                dataSubject.take(1).map {
                    when {
                        it is CurrenciesRatesList -> it
                        it is java.lang.Exception -> throw it
                        else -> throw Exception("This should not happen")
                    }
                }.singleOrError()
            }
        }

        val restartSubject = PublishSubject.create<Unit>()
        val provider = module.provideRatesProvider(mockProvider, restartSubject)
        val testObserver = TestObserver<CurrenciesActivityViewState>()
        provider.subscribe(testObserver)

        dataSubject.onNext(exception0)
        restartSubject.onNext(Unit)

        dataSubject.onNext(data0)
        restartSubject.onNext(Unit)

        dataSubject.onNext(exception1)
        restartSubject.onNext(Unit)

        dataSubject.onNext(data1)
        restartSubject.onNext(Unit)

        dataSubject.onNext(data2)
        restartSubject.onNext(Unit)

        assert(testObserver.values()[0] == CurrenciesActivityViewState.LoadingData)
        assert(
            isCorrectStateAndConstrainsCorrectThrowable(
                testObserver.values()[1],
                exception0,
                CurrenciesActivityViewState.ErrorStateNoData::class
            )
        )
        assert(
            isCorrectStateAndContainsCorrectData(
                testObserver.values()[2],
                data0,
                CurrenciesActivityViewState.DataLoaded::class
            )
        )
        assert(
            isCorrectStateAndConstrainsCorrectThrowable(
                testObserver.values()[3],
                exception1,
                CurrenciesActivityViewState.ErrorStateWeHaveOlderData::class
            )
        )
        assert(
            isCorrectStateAndContainsCorrectData(
                testObserver.values()[4],
                data1,
                CurrenciesActivityViewState.DataLoaded::class
            )
        )
        assert(
            isCorrectStateAndContainsCorrectData(
                testObserver.values()[5],
                data2,
                CurrenciesActivityViewState.DataLoaded::class
            )
        )

    }


    private fun isCorrectStateAndContainsCorrectData(
        state: CurrenciesActivityViewState,
        expectedData: CurrenciesRatesList,
        expectedClass: KClass<*>
    ) =
        when {
            state::class != expectedClass -> false
            state is CurrenciesActivityViewState.RecyclerViewDataProvider -> state.data == expectedData
            else -> false
        }

    private fun isCorrectStateAndConstrainsCorrectThrowable(
        state: CurrenciesActivityViewState,
        expectedThrowable: Throwable,
        expectedClass: KClass<*>
    ) =
        when {
            state::class != expectedClass -> false
            state is CurrenciesActivityViewState.ThrowableDataProvider -> state.throwable == expectedThrowable
            else -> false
        }

}