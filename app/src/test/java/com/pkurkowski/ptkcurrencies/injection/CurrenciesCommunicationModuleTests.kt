package com.pkurkowski.ptkcurrencies.injection

import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.mock
import com.pkurkowski.ptkcurrencies.data.local.CurrenciesRatesProviderInterface
import com.pkurkowski.ptkcurrencies.data.remote.CurrenciesRatesProviderImplementation
import okhttp3.OkHttpClient
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CurrenciesCommunicationModuleTests {

    lateinit var module: CurrenciesCommunicationModule

    @Before
    fun setUp() {
        module = CurrenciesCommunicationModule()
    }

    @Test
    @Throws(Exception::class)
    fun `provide OkHttpClient returns exactly OkHttpClient`() {
        val provide = module.providesHttpClient()
        Assertions.assertThat(provide).isExactlyInstanceOf(OkHttpClient::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `providesGson  returns exactly Gson`() {
        val provide = module.providesGson()
        Assertions.assertThat(provide).isExactlyInstanceOf(Gson::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `providesConverterFactory  returns exactly GsonConverterFactory`() {
        val provide = module.providesConverterFactory(module.providesGson())
        Assertions.assertThat(provide).isExactlyInstanceOf(GsonConverterFactory::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `providesRxJavaCallAdapterFactory returns exactly RxJava2CallAdapterFactory`() {
        val provide = module.providesRxJavaCallAdapterFactory(mock())
        Assertions.assertThat(provide).isExactlyInstanceOf(RxJava2CallAdapterFactory::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun `providesCurrenciesRatesProvider returns exactly UsdPlnDataProviderImplementation`() {
        val provide = module.providesCurrenciesRatesProvider(
            module.providesConverterFactory(module.providesGson()),
            module.providesHttpClient(),
            module.providesRxJavaCallAdapterFactory(mock())
        )
        Assertions.assertThat(provide)
            .isExactlyInstanceOf(CurrenciesRatesProviderImplementation::class.java)
    }

}